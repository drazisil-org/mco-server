// mco-server is a game server, written from scratch, for an old game
// Copyright (C) <2017-2018>  <Joseph W Becher>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import fs from 'fs'
import ini from 'ini'
import path from 'path'

/**
 * Reads the INI configuration file
 * @module
 */

const config = ini.parse(fs.readFileSync(path.join(process.cwd(), 'config/app_settings.ini'), 'utf8'))

/** @type {IAppSettings} */
export const appSettings = {
  serverConfig: {
    certFilename: path.join(process.cwd(), config.serverConfig.certFilename),
    ipServer: config.serverConfig.ipServer,
    privateKeyFilename: path.join(process.cwd(), config.serverConfig.privateKeyFilename),
    publicKeyFilename: path.join(process.cwd(), config.serverConfig.publicKeyFilename),
    connectionURL: config.serverConfig.connectionURL
  }
}
